package main

import (
	"ginorigin-examples/auth"
	"ginorigin-examples/basichttp"
	"ginorigin-examples/component"
	"ginorigin-examples/tasks"
	"ginorigin-examples/teach"

	"gitee.com/chenhonghua/ginorigin/config"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"
)

func main() {
	config.RunServer(load) // 传入自定义的业务，如api、service、dao等
}

func load() {
	zap.LOGGER.Debug("demo.load")

	// demo
	// 添加案例api
	basichttp.Load()
	auth.Load()
	component.Load()
	teach.Load()
	tasks.Load()
}
