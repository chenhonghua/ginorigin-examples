[TOC]

## GINORIGIN

### 项目描述

此项目，是微框架ginorigin的样例项目。

### 项目使用

-   #### DEMO

    #### [ginorigin-examples](https://gitee.com/chenhonghua/ginorigin-examples)

-   #### 引入项目

    ```
    go get gitee.com/chenhonghua/ginorigin
    ```

-   #### main.go

    ```go
    func main() {
    	config.RunServer(load) // 传入自定义的业务，如api、service、dao等
    }
    
    func load() {
         ......
    }
    ```

### 项目打包

- #### Linux

- ```bash
  bash build.sh
  ```

- #### Window

- ```powershell
  build.cmd
  ```

### 项目运行

#### 环境准备

- `golang 1.17`

#### 依赖

```bash
export CGO_ENABLED=0
export GOARCH=amd64
export GOOS=linux
go clean
go get
go get -u github.com/swaggo/swag/cmd/swag
swag init --parseDependency --parseInternal
```

#### 源码运行

```bash
go run main.go
# 或使用"-c"指定配置文件路径
go run main.go -c ./config.yaml
```

#### 二进制包运行

- Linux
  
   - ```bash
     ./ginorigin-examples.linux.x86_64
     # 或使用"-c"指定配置文件路径
     ./ginorigin-examples.linux.x86_64 -c ./config.yaml
     ```

- Window
  
   - 双击此文件运行
   
   - 或使用代码运行
   
   - ```bash
     ginorigin-examples.windows.x86_64.exe
     # 或使用"-c"指定配置文件路径
     ginorigin-examples.windows.x86_64.exe -c ./config.yaml
     ```



