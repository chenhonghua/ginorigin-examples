package component

import (
	"strconv"
	"time"

	"gitee.com/chenhonghua/ginorigin/config/http/restful"
	"gitee.com/chenhonghua/ginorigin/config/http/router"
	"gitee.com/chenhonghua/ginorigin/config/storage/redis"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"

	"github.com/gin-gonic/gin"
)

type componentRedisDemo struct{}

func (crd componentRedisDemo) load() {
	iroutes := router.GetIRoutes("examples/component")
	iroutes.GET("redis/test1", crd.componentRedisDemoApiHandler1)
	iroutes.GET("redis/test2", crd.componentRedisDemoApiHandler2)
}

// 组件测试：redis
// @Tags component
// @Summary 组件测试：redis
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{},msg=string} "组件测试：redis(基础字符串存储)"
// @Router /examples/component/redis/test1 [get]
func (componentRedisDemo) componentRedisDemoApiHandler1(c *gin.Context) {
	i := 0
	for i < 5 {
		i++
		n := time.Now()
		j, _ := n.MarshalJSON()
		zap.LOGGER.Debug("redis添加数据成功"+strconv.Itoa(i)+":", zap.Any("s", redis.Default.SetEx(strconv.Itoa(i), string(j), 6)))
	}
	zap.LOGGER.Debug("redis获取键列表：", zap.Any("s", redis.Default.Keys("*")))
	s, err := redis.Default.Get("1")
	zap.LOGGER.Debug("redis获取数据成功1：", zap.Any("s", s), zap.Error(err))
	zap.LOGGER.Debug("休眠5秒")
	time.Sleep(time.Duration(5) * time.Second)
	zap.LOGGER.Debug("测试查看过期时间1:", zap.Any("s", redis.Default.TTL("1")))
	zap.LOGGER.Debug("测试设置过期时间1:", zap.Any("s", redis.Default.Expire("1", 10)))
	zap.LOGGER.Debug("测试查看过期时间1:", zap.Any("s", redis.Default.TTL("1")))
	zap.LOGGER.Debug("休眠5秒")
	time.Sleep(time.Duration(5) * time.Second)
	zap.LOGGER.Debug("测试查看是否存在1/2/3:", zap.Any("s", redis.Default.Exists("1")))
	zap.LOGGER.Debug("测试删除1:", zap.Any("s", redis.Default.Del("1")))
	zap.LOGGER.Debug("测试查看是否存在1:", zap.Any("s", redis.Default.Exists("1")))
	restful.Success.WithMessage(c, "样例接口1")
}

// 组件测试：redis
// @Tags component
// @Summary 组件测试：redis
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{},msg=string} "组件测试：redis(hash存储)"
// @Router /examples/component/redis/test2 [get]
func (componentRedisDemo) componentRedisDemoApiHandler2(c *gin.Context) {
	hashname := "demohash"
	i := 0
	for i < 5 {
		i++
		n := time.Now()
		j, _ := n.MarshalJSON()
		zap.LOGGER.Debug("redis添加数据成功"+strconv.Itoa(i)+":", zap.Any("s", redis.Hash.HSetNX(hashname, strconv.Itoa(i), string(j))))
	}
	n := time.Now()
	j, _ := n.MarshalJSON()
	zap.LOGGER.Debug("redis重复添加数据成功"+strconv.Itoa(i)+":", zap.Any("s", redis.Hash.HSetNX(hashname, strconv.Itoa(1), string(j))))
	zap.LOGGER.Debug("redis获取键列表成功1：", zap.Any("s", redis.Hash.HKeys(hashname)))
	zap.LOGGER.Debug("测试查看是否存在1", zap.Any("s", redis.Hash.HExists(hashname, "1")))
	zap.LOGGER.Debug("redis获取数据成功1：", zap.Any("s", redis.Hash.HGet(hashname, "1")))
	zap.LOGGER.Debug("redis删除数据成功1：", zap.Any("s", redis.Hash.HDel(hashname, "1")))
	zap.LOGGER.Debug("redis获取键列表失败1：", zap.Any("s", redis.Hash.HKeys(hashname)))
	i = 0
	for i < 5 {
		i++
		n := time.Now()
		j, _ := n.MarshalJSON()
		if e := redis.Hash.HSet(hashname, strconv.Itoa(i), string(j)); e != nil {
			zap.LOGGER.Error("redis覆盖添加数据失败"+strconv.Itoa(i)+"：", zap.Error(e))
		} else {
			zap.LOGGER.Debug("redis覆盖添加数据成功" + strconv.Itoa(i))
		}
	}
	zap.LOGGER.Debug("redis获取数据成功1：", zap.Any("s", redis.Hash.HGet(hashname, "1")))
	zap.LOGGER.Debug("redis获取数据成功1：", zap.Any("s", redis.Hash.HVals(hashname)))
	restful.Success.WithMessage(c, "样例接口1")
}
