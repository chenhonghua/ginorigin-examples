package component

import (
	"time"

	"gitee.com/chenhonghua/ginorigin/config/http/router"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"

	"gitee.com/chenhonghua/ginorigin/config/storage/oos/minio"

	"github.com/gin-gonic/gin"
)

type componentMinioDemo struct{}

func (m componentMinioDemo) load() {
	iroutes := router.GetIRoutes("examples/component")
	iroutes.GET("minio/test1", m.componentMinioDemoApiHandler1)
}

// 组件测试：minio
// @Tags component
// @Summary 组件测试：minio
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{},msg=string} "组件测试：minio"
// @Router /examples/component/minio/test1 [get]
func (componentMinioDemo) componentMinioDemoApiHandler1(c *gin.Context) {
	bucket := minio.Bucket{Name: "ginorigin"}
	zap.LOGGER.Debug("文件桶清单：", zap.Any("arr", bucket.List("/", true)))
	stat, _ := bucket.Stat("/d1/d2/d4/test2202242325.txt")
	zap.LOGGER.Debug("对象信息：", zap.Any("stat", stat))
	// bucket.Put("./testfile.txt", "/d1/d2/d4/test2202242324.txt")
	// bucket.Put("./testfile.txt", "/d1/d2/d4/test2202242325.txt")
	// bucket.Get("/d1/d2/d4/test2202242325.txt", ".//d1/d2/d4/test2202242325.txt")
	// bucket.Delete("/d1/d2/d4/test2202242324.txt")
	u, _ := bucket.PresignedGet("/d1/d2/d4/test2202242325.txt", "tetetetete.txt", time.Duration(1)*time.Minute)
	zap.LOGGER.Debug("创建下载分享链：", zap.Any("url", u))
	u, _ = bucket.PresignedPut("/d1/d2/d4/test2202250009.txt", time.Duration(1)*time.Hour)
	zap.LOGGER.Debug("创建上传分享链：", zap.Any("url", u))
}
