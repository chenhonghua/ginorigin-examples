package component

import ()

func Load() {
	componentDatabaseDemo{}.load()   // 数据库相关案例
	componentLocalcacheDemo{}.load() // localcache相关的案例
	componentRedisDemo{}.load()      // redis相关的案例
	componentKafkaDemo{}.load()      // kafka相关的案例
	componentMinioDemo{}.load()      // minio相关的案例
}
