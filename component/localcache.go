package component

import (
	"strconv"
	"time"

	"gitee.com/chenhonghua/ginorigin/config/http/restful"
	"gitee.com/chenhonghua/ginorigin/config/http/router"
	"gitee.com/chenhonghua/ginorigin/config/storage/localcache"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"

	"github.com/gin-gonic/gin"
)

type componentLocalcacheDemo struct{}

func (d componentLocalcacheDemo) load() {
	iroutes := router.GetIRoutes("examples/component")
	iroutes.GET("localcache/test1", d.componentLocalcacheDemoApiHandler1)
}

// 组件测试：localcache
// @Tags component
// @Summary 组件测试：localcache
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{},msg=string} "组件测试：localcache"
// @Router /examples/component/localcache/test1 [get]
func (componentLocalcacheDemo) componentLocalcacheDemoApiHandler1(c *gin.Context) {
	i := 0
	for i < 5 {
		i++
		n := time.Now()
		j, _ := n.MarshalJSON()
		localcache.Default.Set(strconv.Itoa(i), string(j))
		zap.LOGGER.Debug("缓存添加数据成功")
	}
	s, b := localcache.Default.Get("1")
	zap.LOGGER.Debug("缓存获取数据1：", zap.Any("s", s), zap.Any("b", b))
	zap.LOGGER.Debug("缓存获取全部数据：", zap.Any("s", localcache.Default.GetAll()))
	localcache.Default.Del("1")
	zap.LOGGER.Debug("缓存删除数据1：", zap.Any("s", localcache.Default.GetAll()))
	restful.Success.WithMessage(c, "样例接口1")
}
