package tasks

import (
	"gitee.com/chenhonghua/ginorigin/config/other/scheduler"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"
)

func Load() {
	// StartTask1()
	// StartTask2()
}

func StartTask1() {
	scheduler.Job{
		Name: "demojob01",
		Spec: "*/2 * * * * ?",
		Func: func() {
			zap.LOGGER.Info("执行样例任务demojob01")
		},
	}.Start()
}

func StartTask2() {
	scheduler.Job{
		Name: "demojob02",
		Spec: "*/3 * * * * ?",
		Func: func() {
			zap.LOGGER.Info("执行样例任务demojob02")
		},
	}.Start()
}
