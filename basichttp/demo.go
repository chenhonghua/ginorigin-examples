package basichttp

import (
	"mime/multipart"
	"time"

	"gitee.com/chenhonghua/ginorigin/config/http/restful"
	"gitee.com/chenhonghua/ginorigin/config/http/router"
	"gitee.com/chenhonghua/ginorigin/config/storage/database"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"

	"github.com/gin-gonic/gin"
)

// 此类下的接口，是无需认证拦截的接口，用于演示gin框架的api接口调度及swagger用法
// 在代码根目录下，使用命令
//     go get -u github.com/swaggo/swag/cmd/swag
//     swag init --parseDependency --parseInternal
// 能根据注解自动化生成swagger代码（/docs目录），而后，在config.yaml中开启swagger，
// 即可通过浏览器访问http://host:port/swagger/index.html，来查看系统的api了
// swagger注解：@Tags <分类标签>
// swagger注解：@Summary <接口摘要>
// swagger注解：@Description <接口详细描述>
// swagger注解：@Param <参数名> <参数种类> <参数数据类型> <是否必须参数> <参数描述>
//   参数种类:
//     formData		post 请求的数据
//     query		url查询参数，如/examples?param1=xxx&param2=xxx
//     path			url路径参数，如/examples/param1/param2
//     body			raw数据
//     header		带在 header 信息中得参数
// swagger注解：@Produce  <接口context-type>
// swagger注解：@Success <http-status> <返回类型> <返回类型描述表达式> <响应描述>
//   返回类型:
//     {object}		对象类型数据
//   返回类型描述表达式:对第二个参数的消息描述，比如{object}类型返回时，使用对象json进行详细描述
// swagger注解：@Router <uri> [<method，多个时用","分割>]
type demo1Api struct{}

func Load() {
	api := demo1Api{}
	noAuthIRoutes := router.GetIRoutes("examples")
	noAuthIRoutes.GET("d1h1", api.demo1ApiHandler1)
	noAuthIRoutes.GET("d1h2", api.demo1ApiHandler2)
	noAuthIRoutes.POST("d1h3", api.demo1ApiHandler3)
	noAuthIRoutes.GET("d1h4/:param1/:param2", api.demo1ApiHandler4) // 路径参数
	noAuthIRoutes.GET("d1h5", api.demo1ApiHandler5)
	noAuthIRoutes.POST("d1h6", api.demo1ApiHandler6)
	noAuthIRoutes.POST("d1h7", api.demo1ApiHandler7)
}

// @Tags demo
// @Summary 这个就是api的helloworld
// @Description 这个就是api的helloworld
// @Produce  application/json
// @Success 200 {object} restful.Response{msg=string}
// @Router /examples/d1h1 [get]
func (demo1Api) demo1ApiHandler1(c *gin.Context) {
	restful.Success.WithMessage(c, "样例接口1")
}

// @Tags demo
// @Summary 演示对象数据返回
// @Description 演示对象数据返回
// @Produce  application/json
// @Success 200 {object} restful.Response{data=GinOriginDemoModel3}
// @Router /examples/d1h2 [get]
func (demo1Api) demo1ApiHandler2(c *gin.Context) {
	// v, b := c.Get("testtest")
	// zap.LOGGER.Debug("c.Get=", zap.Any("v", v), zap.Any("b", b))
	m := GinOriginDemoModel3{
		GinOriginDemoModel1: GinOriginDemoModel1{Name: "ginorigin2"},
		GinOriginDemoModel2: GinOriginDemoModel2{
			BaseModel: database.BaseModel{
				CreatedAt: database.NewNullTime(time.Now()),
			},
		},
	}
	restful.Success.WithData(c, m)
}

type HeaderReq struct {
	Param1 string
	Param2 string
	Param3 string
}

// @Tags demo
// @Summary 演示header参数获取
// @Description 演示header参数获取
// @Produce  application/json
// @Success 200 {object} restful.Response{data=HeaderReq}
// @Router /examples/d1h3 [post]
func (demo1Api) demo1ApiHandler3(c *gin.Context) {
	param1 := c.GetHeader("param1")
	hp := HeaderReq{}
	c.ShouldBindHeader(&hp)
	zap.LOGGER.Debug("param1=" + param1)
	zap.LOGGER.Debug("HeaderReq=", zap.Any("HeaderReq", hp))
	restful.Success.WithData(c, hp)
}

// @Tags demo
// @Summary 演示path参数获取
// @Description 演示path参数获取
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{}}
// @Router /examples/d1h4/:param1/:param2 [get]
func (demo1Api) demo1ApiHandler4(c *gin.Context) {
	param1 := c.Param("param1")
	param2 := c.Param("param2")
	zap.LOGGER.Debug("path param = ", zap.Any("param1", param1), zap.Any("param2", param2))
	restful.Success.WithData(c, map[string]interface{}{
		"param1": param1,
		"param2": param2,
	})
}

// @Tags demo
// @Summary 演示query参数获取
// @Description 演示query参数获取
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{}}
// @Router /examples/d1h5 [get]
func (demo1Api) demo1ApiHandler5(c *gin.Context) {
	param1 := c.Query("param1")
	param2 := c.Query("param2")
	zap.LOGGER.Debug("query param = ", zap.Any("param1", param1), zap.Any("param2", param2))
	restful.Success.WithData(c, map[string]interface{}{
		"param1": param1,
		"param2": param2,
	})
}

// @Tags demo
// @Summary 演示rawjson参数获取
// @Description 演示rawjson参数获取
// @Produce  application/json
// @Success 200 {object} restful.Response{data=GinOriginDemoModel3}
// @Router /examples/d1h6 [post]
func (demo1Api) demo1ApiHandler6(c *gin.Context) {
	m := GinOriginDemoModel3{}
	err := c.ShouldBindJSON(&m)
	if nil != err {
		restful.Failed.WithError(c, err)
		return
	}
	zap.LOGGER.Debug("raw json = ", zap.Any("GinOriginDemoModel3", m))
	restful.Success.WithData(c, m)
}

// @Tags demo
// @Summary 演示form参数获取
// @Description 演示form参数获取
// @Produce  application/json
// @Success 200 {object} restful.Response{data=map[string]interface{}}
// @Router /examples/d1h7 [post]
func (demo1Api) demo1ApiHandler7(c *gin.Context) {
	formDatas, err := c.MultipartForm()
	if nil != err {
		restful.Failed.WithError(c, err)
		return
	}
	var files map[string][]*multipart.FileHeader = formDatas.File // 文件
	var textForm map[string][]string = formDatas.Value            // 文本表单
	zap.LOGGER.Debug("form = ", zap.Any("files", files), zap.Any("textForm", textForm))
	restful.Success.WithData(c, textForm)
}
