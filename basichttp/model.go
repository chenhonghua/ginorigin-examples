package basichttp

import "gitee.com/chenhonghua/ginorigin/config/storage/database"

type GinOriginDemoModel1 struct {
	Name string `json:"name" gorm:"column:name;type:char(128);index;unique;not null;comment:名字"`
}

// 自定义表名
func (GinOriginDemoModel1) TableName() string {
	return "gin_origin_demo_model1"
}

type GinOriginDemoModel2 struct {
	database.BaseModel
}

// 自定义表名
func (GinOriginDemoModel2) TableName() string {
	return "gin_origin_demo_model2"
}

type GinOriginDemoModel3 struct {
	GinOriginDemoModel2
	GinOriginDemoModel1
}

// 自定义表名
func (GinOriginDemoModel3) TableName() string {
	return "gin_origin_demo_model3"
}
