package teach

import (
	"strconv"

	"gitee.com/chenhonghua/ginorigin/config/http/restful"
	"gitee.com/chenhonghua/ginorigin/config/system/zap"

	"github.com/gin-gonic/gin"
)

type TeachApi struct{}

// @Tags teach
// @Summary 新增课程
// @Description 新增课程
// @Produce  application/json
// @Success 200 {object} restful.Response{data=Course}
// @Router /examples/teach/course [post]
func (i *TeachApi) CourseAdd(c *gin.Context) {
	p := Course{}
	c.ShouldBindJSON(&p)
	p = p.Create()
	restful.Success.WithData(c, p)
}

// @Tags teach
// @Summary 更新课程
// @Description 更新课程
// @Produce  application/json
// @Success 200 {object} restful.Response{data=Course}
// @Router /examples/teach/course [put]
func (i *TeachApi) CourseUpdate(c *gin.Context) {
	p := Course{}
	c.ShouldBindJSON(&p)
	err := p.Update()
	if err != nil {
		restful.Failed.WithError(c, err)
		return
	}
	restful.Success.WithData(c, p)
}

// @Tags teach
// @Summary 删除课程
// @Description 删除课程
// @Produce  application/json
// @Success 200 {object} restful.Response{data=Course}
// @Router /examples/teach/course [delete]
func (i *TeachApi) CourseDel(c *gin.Context) {
	p := Course{}
	c.ShouldBindJSON(&p)
	err := p.Delete()
	if err != nil {
		restful.Failed.WithError(c, err)
		return
	}
	restful.Success.WithData(c, p)
}

// @Tags teach
// @Summary 查询课程详情
// @Description 查询课程详情
// @Produce  application/json
// @Success 200 {object} restful.Response{data=Course}
// @Router /examples/teach/course [get]
func (i *TeachApi) Course(c *gin.Context) {
	id := c.Query("id")
	zap.LOGGER.Debug("查询课程详情:", zap.Any("id", id))
	// st := time.Now().UnixMicro()
	r, err := Course{}.FindByCondition("id", id)
	// lt := time.Now().UnixMicro() - st // 350-450us
	// 无日志耗时350-450us(redis缓存)
	// zap.LOGGER.Debug("查询课程详情:")                                       // 日志debug判定不输出接口耗时400-450us
	// zap.LOGGER.Debug("查询课程详情:", zap.Any("r", r), zap.Any("err", err)) // 日志debug判定不输出接口耗时400-450us
	// zap.LOGGER.Info("查询课程详情:")                                        // 简易日志接口耗时600-700-1000us
	// zap.LOGGER.Info("查询课程详情:", zap.Any("r", r), zap.Any("err", err))  // 完整日志接口耗时700-800-1000us
	// 两行debug不输出 + 两行info输出，接口耗时800-1000
	if err != nil {
		restful.Failed.WithError(c, err)
		return
	}
	// m := map[string]interface{}{"lt": lt, "Course": r}
	restful.Success.WithData(c, r)
}

// @Tags teach
// @Summary 查询课程列表
// @Description 查询课程列表
// @Produce  application/json
// @Success 200 {object} restful.Response{data=[]Course}
// @Router /examples/teach/courses [get]
func (i *TeachApi) Courses(c *gin.Context) {
	q := Course{}
	c.ShouldBindJSON(&q)
	zap.LOGGER.Debug("查询条件:", zap.Any("parameters", q))
	r, err := q.FindBySelf()
	if err != nil {
		restful.Failed.WithError(c, err)
		return
	}
	restful.Success.WithData(c, r)
}

type Page struct {
	Data  []Course
	Page  int
	Size  int
	Total int64
}

// @Tags teach
// @Summary 分页查询课程
// @Description 分页查询课程
// @Produce  application/json
// @Success 200 {object} restful.Response{data=Page}
// @Router /examples/teach/courses/page [get]
func (i *TeachApi) PageCourses(c *gin.Context) {
	page, _ := strconv.Atoi(c.Param("page"))
	size, _ := strconv.Atoi(c.Param("size"))
	zap.LOGGER.Debug("查询条件:", zap.Any("page", page), zap.Any("size", size))
	r, t, err := Course{}.PageBySelf(page, size)
	if err != nil {
		restful.Failed.WithError(c, err)
		return
	}
	restful.Success.WithData(c, Page{
		Data:  r,
		Page:  page,
		Size:  size,
		Total: t,
	})
}
