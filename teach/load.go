package teach

import (
	"gitee.com/chenhonghua/ginorigin/config/http/router"
	"gitee.com/chenhonghua/ginorigin/config/http/security"
)

func Load() {
	createTalbes()   // 建表
	importMetaData() // 导入元数据
	t := TeachApi{}
	iRoutes := router.GetIRoutes("examples/teach")
	iRoutes.POST("course", t.CourseAdd)
	iRoutes.PUT("course", t.CourseUpdate)
	iRoutes.DELETE("course", t.CourseDel)

	iRoutes.GET("course/noauth", t.Course)
	// iRoutes.GET("courses", t.Courses)
	// iRoutes.GET("courses/:page/:size", t.PageCourses)

	security.Router{Method: security.MethodGet, SubTag: "教学模块课程查询", RelativePath: "/examples/teach/course", Handler: t.Course}.Regist()
	security.Router{Method: security.MethodGet, SubTag: "教学模块课程查询", RelativePath: "/examples/teach/courses", Handler: t.Courses}.Regist()
	security.Router{Method: security.MethodGet, SubTag: "教学模块课程查询", RelativePath: "/examples/teach/courses/:page/:size", Handler: t.PageCourses}.Regist()

}
